﻿using Exam.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Exam
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Login : Window
    {
        private LoginController _Controller;

        public Login()
        {
            InitializeComponent();
            InitComponet();
        }

        private void InitializeComponent()
        {
            throw new NotImplementedException();
        }

        public void InitComponet()
        {
            _Controller = new LoginController(this);
            NameSingUp.KeyDown += new KeyEventHandler(_Controller._KeyDownTextBox);
            NameLoginIn.KeyDown += new KeyEventHandler(_Controller._KeyDownTextBox);
            ButtonExit.Click += new RoutedEventHandler(_Controller.ButtonRoutedEventArgs);
            ButtonLogin.Click += new RoutedEventHandler(_Controller.ButtonRoutedEventArgs);
            ButtonSingUp.Click += new RoutedEventHandler(_Controller.ButtonRoutedEventArgs);
            ButtonRegistro.Click += new RoutedEventHandler(_Controller.ButtonRoutedEventArgs);
            ButtonVolverLogin.Click += new RoutedEventHandler(_Controller.ButtonRoutedEventArgs);
        }

        private void GridPrincipalMouseDown(object sender, MouseButtonEventArgs e) => DragMove();
    }


}
